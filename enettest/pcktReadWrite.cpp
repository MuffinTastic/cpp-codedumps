#include "pcktReadWrite.h"

#include <cstring>
#include <string>

#define CHAR_SIZE 1
#define INT_SIZE 4
#define FLOAT_SIZE 4
#define BOOL_SIZE 1

static char* packetData = NULL;
static int headerPos = 0;
static bool mallocBlock = false;

void packetReadWrite::reset()
{
	packetData = NULL;
	headerPos = 0;
	if (mallocBlock)
		free(packetData);
}

void packetReadWrite::setData(char* data)
{
	packetData = data;
	headerPos = 0;
}

int packetReadWrite::getPosition()
{
	return headerPos;
}

char packetReadWrite::readChar()
{
	char data = *reinterpret_cast<char*>(packetData + headerPos);
	headerPos += CHAR_SIZE;
	return data;
}

int packetReadWrite::readInt()
{
	int data = *reinterpret_cast<int*>(packetData + headerPos);
	headerPos += INT_SIZE;
	return data;
}

float packetReadWrite::readFloat()
{
	float data = *reinterpret_cast<float*>(packetData + headerPos);
	headerPos += FLOAT_SIZE;
	return data;
}

bool packetReadWrite::readBool()
{
	bool data = *reinterpret_cast<bool*>(packetData + headerPos);
	headerPos += BOOL_SIZE;
	return data;
}

char* packetReadWrite::readString(int size)
{
	char* data = reinterpret_cast<char*>(packetData + headerPos);
	headerPos += size + 1;
	return data;
}


void packetReadWrite::writeChar(char data)
{
	memcpy(packetData + headerPos, &data, CHAR_SIZE);
	headerPos += CHAR_SIZE;
}

void packetReadWrite::writeInt(int data)
{
	memcpy(packetData + headerPos, &data, INT_SIZE);
	headerPos += INT_SIZE;
}

void packetReadWrite::writeFloat(float data)
{
	memcpy(packetData + headerPos, &data, FLOAT_SIZE);
	headerPos += FLOAT_SIZE;
}

void packetReadWrite::writeBool(bool data)
{
	memcpy(packetData + headerPos, &data, BOOL_SIZE);
	headerPos += BOOL_SIZE;
}

void packetReadWrite::writeString(char* data, int size)
{
	memcpy(packetData + headerPos, data, size + 1);
	headerPos += size + 1;
}