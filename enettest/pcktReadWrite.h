#pragma once

namespace packetReadWrite {

	void reset();
	void setData(char*);
	int  getPosition();

	char  readChar();
	int   readInt();
	float readFloat();
	bool  readBool();
	char* readString(int size);

	void writeChar(char);
	void writeInt(int);
	void writeFloat(float);
	void writeBool(bool);
	void writeString(char*, int);
}