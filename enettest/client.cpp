// for kiki:
// have fun figuring all this shit out KEK XXDDXDXDXDXD

#include <cstdio>
#include <cstring>
#include <enet/enet.h>

#include "pcktReadWrite.h"

#define HOST "localhost"
#define PORT (32887)
#define BUFFERSIZE (1000)

char  buffer[BUFFERSIZE];

ENetHost  *client;
ENetAddress  address;
ENetEvent  event;
ENetPeer  *peer;
ENetPacket  *packet;

typedef unsigned char BYTE;

typedef struct infoStruct
{
	BYTE playerID;
	BYTE team;
	BYTE weapon;
	BYTE item;
	unsigned int kills;
	BYTE red;
	BYTE green;
	BYTE blue;
	char name[16];
};

typedef union ExistingPlayer
{
	infoStruct packetInfo;
	BYTE packetData[28];
};

int  main(int argc, char ** argv)
{
	int connected = 0;
	char* datablock = (char*)malloc(28);

	if (enet_initialize() != 0) {
		printf("Could not initialize enet.\n");
		getchar();
		return 0;
	}

	client = enet_host_create(NULL, 1, 31, 57600 / 8, 14400 / 8);

	enet_host_compress_with_range_coder(client);

	if (client == NULL) {
		printf("Could not create client.\n");
		getchar();
		return 0;
	}

	enet_address_set_host(&address, HOST);
	address.port = PORT;

	peer = enet_host_connect(client, &address, 31, 3);

	if (peer == NULL) {
		printf("Could not connect to server\n");
		getchar();
		return 0;
	}

	memset(datablock, 0, 28);

	/*
	BYTE playerID;
	BYTE team;
	BYTE weapon;
	BYTE item;
	unsigned int kills;
	BYTE red;
	BYTE green;
	BYTE blue;
	char name[16];
	*/


	int result = enet_host_service(client, &event, 1000);
	if (result > 0 &&
		event.type == ENET_EVENT_TYPE_CONNECT) {

		printf("Connection to %s succeeded.\n", HOST);
		connected++;


		packetReadWrite::setData(buffer);
			packetReadWrite::writeChar(9);
			packetReadWrite::writeChar(0);
			packetReadWrite::writeChar(0);
			packetReadWrite::writeChar(0);
			packetReadWrite::writeChar(2);
			packetReadWrite::writeInt(0);
			packetReadWrite::writeChar(0);
			packetReadWrite::writeChar(0);
			packetReadWrite::writeChar(0);
			packetReadWrite::writeString("stop talking", 12);
		packetReadWrite::reset();


		packet = enet_packet_create(buffer, 28,
			ENET_PACKET_FLAG_RELIABLE);
		enet_peer_send(peer, 0, packet);

	} else {
		if (event.type == ENET_EVENT_TYPE_DISCONNECT) {
			printf("%i %s\n", event.data, event.data);
			printf("%i %s\n", event.peer->data, event.peer->data);
		}
		printf("%i\n", event.type);
		printf("r%i\n", result);
		enet_peer_reset(peer);
		printf("Could not connect to %s.\n", HOST);
		getchar();
		return -1;
	}

	while (1) {
		while (enet_host_service(client, &event, 1000) > 0) {
			switch (event.type) {
			case ENET_EVENT_TYPE_RECEIVE:
				char packetID;
				packetReadWrite::setData((char*)event.packet->data);
				packetID = packetReadWrite::readChar();
				switch (packetID) {
				case 17:
					char playerID, chatType;
					char* message;

					playerID = packetReadWrite::readChar();
					chatType = packetReadWrite::readChar();
					message = packetReadWrite::readString(0);

					printf("Chat detected! ID:%i TYPE:%i MESSAGE:%s\n", playerID, chatType, message);
					if (message[0] == '\x3') { // a heart :)
						packetReadWrite::setData(buffer);
						packetReadWrite::writeChar(17);
						packetReadWrite::writeChar(0);
						packetReadWrite::writeChar(0);
						packetReadWrite::writeString(":D \x3\x3\x3\x3\x3", 9);
						packet = enet_packet_create(buffer, 12,
							ENET_PACKET_FLAG_RELIABLE);
						enet_peer_send(peer, 0, packet);
						enet_host_flush(client);
					}
				default:
					break;
				}
				packetReadWrite::reset();
				break;
			case ENET_EVENT_TYPE_DISCONNECT:
				connected = 0;
				printf("You have been disconnected.\n");
				getchar();
				return 2;
			}
		}
	}

	enet_deinitialize();
	getchar();
	free(datablock);
	return 0;
}